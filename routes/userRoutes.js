const express = require('express');
const router = express.Router();
const userController = require('../controllers/usercontroller')
const auth = require("../auth");




// User Registration with validation for checking duplicates.

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//logIn user

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})



// Retrieving User details Route
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	userController.getUserDetails({id: userData.id}).then(resultFromController => res.send(resultFromController))
});



// Route for Setting user as an admin

router.put("/setAdmin", auth.verify, (req, res) => { 
 
	const userData = auth.decode(req.headers.authorization) 
 
	if(userData.isAdmin){ 
 
			userController.setAdmin(req.body).then(resultFromController => res.send(resultFromController)) 
	} else { 
		 
		res.send({auth:"failed"}) 
	} 
}); 







module.exports = router