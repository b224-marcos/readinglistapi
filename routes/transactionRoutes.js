const express = require('express');
const router = express.Router();
const transactionController = require('../controllers/transactionController');
const auth = require("../auth");


// Non-admin Booking route
router.post("/booking", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization) 

    const data = {
        userId: req.body.userId,
        roomName: req.body.roomName,
        roomId: req.body.roomId,
        quantity: req.body.quantity
    }

if(userData.isAdmin){ 
 
    res.send({message: "Admin users not allowed to book"}) 
            } else { 

    transactionController.bookRoom(data).then((resultFromController) => {
        res.send(resultFromController)
    }) 

}
});

// Retrieve all Transactions (Admin only)
router.get("/all-transactions", auth.verify, (req, res) => {
   
   const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin) {
        transactionController.getAllTransactions(userData).then(resultFromController => res.send(resultFromController))
    } else {
        res.send({message: "User not an admin / Cannot retrieve transactions"})
    }
})





module.exports = router