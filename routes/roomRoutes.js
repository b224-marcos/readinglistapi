const express = require('express');
const router = express.Router();
const roomController = require('../controllers/roomController');
const auth = require("../auth")




// Creating a Room

router.post("/addRoom", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
	roomController.addRoom(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
})



// Retrieve all Rooms

router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		roomController.getAllRooms(userData).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({message: "You are not an admin"})
	}
});


// Retrieve all available rooms

router.get("/", (req, res) => { 
	roomController.getAllAvailable().then(resultFromController => res.send(resultFromController))
}); 


// Route for retrieving specific room

router.get("/:roomId", (req, res) => {
	console.log(req.params.roomId)

	roomController.getRoom(req.params).then(resultFromController => res.send(resultFromController))
});


//Route for updating a Room

router.put("/:roomId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		roomController.UpdateRoom(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}

})


// Archiving a Room Route

router.put("/archive/:roomId", auth.verify, (req, res) => {

     const userData = auth.decode(req.headers.authorization)

     if(userData.isAdmin) {
         roomController.archiveRoom(req.params, req.body).then(resultFromController => res.send(resultFromController))
     } else {
         res.send({auth: "failed / Not an admin"})
     }

 });


// Activate Room route

router.put("/activate/:roomId", auth.verify, (req, res) => {

     const userData = auth.decode(req.headers.authorization)

     if(userData.isAdmin) {
         roomController.activateRoom(req.params, req.body).then(resultFromController => res.send(resultFromController))
     } else {
         res.send({auth: "failed / Not an admin"})
     }

 });





module.exports = router

