const mongoose = require('mongoose');


const roomSchema = new mongoose.Schema ({

	name: {
		type: String,
		required: true
	},

	description: {
		type: String,
		reqired: true
	},

	price: {
		type: Number,
		required: true
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	}
});





module.exports = mongoose.model('Room', roomSchema)