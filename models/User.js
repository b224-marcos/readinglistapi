const mongoose = require('mongoose');



const userSchema = new mongoose.Schema ({

	firstName: {
		type: String,
		required: [true, "Please add a name"],
	},

	lastName: {
		type: String,
		required: [true, "Please add a last name"]
	},

	mobileNo: String,

	email: {
		type: String,
		required: [true, "Please add an email"]
		
	},

	password: {
		type: String,
		required: [ true, "Password required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	}


});




module.exports = mongoose.model ('User', userSchema)