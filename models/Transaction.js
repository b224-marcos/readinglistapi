const mongoose = require ('mongoose');


const transactionSchema = new mongoose.Schema ({

	userId: {
		type: String,
		required: true
	},

	roomName: {
		type: String,
		required: true
	},

	roomBooking: [{
		
		roomId: {
			type: String,
			required: true
		},

		quantity: {
			type: Number,
			required: true
		}
	}],

	totalAmount: Number,

	purchasedOn: {
		type: Date,
		deafult: new Date()
	}



});




module.exports = mongoose.model('Transaction', transactionSchema)