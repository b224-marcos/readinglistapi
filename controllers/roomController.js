const Room = require('../models/Room');




// Creating a Room

module.exports.addRoom = (reqBody) => {

	let newRoom = new Room ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	});

	return newRoom.save().then((room, error) => {
		if (error) {
			return ({message: "Room creation failed"})
		} else {
			return ({message: "Room Creation successful"})
		}
	})
};


// Retrieving all Rooms


module.exports.getAllRooms = (data) => {
	
	return Room.find({}).then((result, error) => {
			if(error) {
				return false
			} else {
				return result
			}
		})
	};


// Retrieve all available rooms

module.exports.getAllAvailable = () => {

	return Room.find({isActive: true}).then(result => {
		return result
	})
};


// Function for retrieving a specific room

module.exports.getRoom = (reqParams) => {

	return Room.findById(reqParams.roomId).then(result => {

		return result
	})
};

// Function for updating a specific Room

module.exports.UpdateRoom = (reqParams, reqBody) => {

	let updatedRoom = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Room.findByIdAndUpdate(reqParams.roomId, updatedRoom).then((updatedRoom, error) => {

		console.log(updatedRoom)
		if(error) {
			return false
		} else {
			return true
		}
	})
};

// Function for Archiving a Room
 module.exports.archiveRoom = (reqParams, reqBody) => {
    
    let archiveRoom = {
        isActive: reqBody.isActive
 }
    return Room.findByIdAndUpdate(reqParams.roomId, archiveRoom).then((archiveRoom, error) => {
               
       if(error) {
           return false
      } else {

      	
            return true
           }
      })
};

// Function for activating a Room

module.exports.activateRoom = (reqParams, reqBody) => {
    
    let activateRoom = {
        isActive: reqBody.isActive
 }
    return Room.findByIdAndUpdate(reqParams.roomId, activateRoom).then((activateRoom, error) => {
               
       if(error) {
           return false
      } else {
            return true
           }
      })
};

