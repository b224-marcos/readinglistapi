const Transaction = require('../models/Transaction');
const User = require('../models/User');
const Room = require('../models/Room')
const bcrypt = require('bcrypt')
const auth = require('../auth')



// Non-admin Booking
module.exports.bookRoom = (data) => {
       
        let new_booking = new Transaction({
           
            userId: data.userId,
            roomName: data.roomName,
            roomBooking: {
                roomId: data.roomId,
                quantity: data.quantity
            }
           
        });

        return new_booking.save().then((booked, error) => {
            if(error){
                return false
            }

            return ({ message: "Booking Successful" })
        })
    };


// Retrieve all orders (Admin only)
module.exports.getAllTransactions = (userData) => {
    return Transaction.find({}).then((result, error) => {
        if(error) {
            return false
        } else {
            return result
        }
    })
};




