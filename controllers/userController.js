const User = require('../models/User');
const Room = require('../models/Room')
const auth = require("../auth"); 
const bcrypt = require("bcrypt"); 







// Register User 
module.exports.registerUser = (reqBody) => { 
	let newUser = new User({ 
		firstName: reqBody.firstName, 
		lastName: reqBody.lastName, 
		mobileNo: reqBody.mobileNo, 
		email: reqBody.email, 		
		password: bcrypt.hashSync(reqBody.password, 10) 
	}) 
 
	
	return User.findOne({email: reqBody.email}).then(result => { 
 
		if(result){ 
			 
			return ({message: "User already registered"})
		 
		} else { 
 
			return newUser.save().then((user, error) => { 
 
				if(error){ 
					return false 
				} else { 
					return true
				} 
			}) 
 
		} 
	})	 
}; 


// user login

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}


// retrieve user details

module.exports.getUserDetails = (userData) => {
	return User.findById(userData.id).then(result => {
				
		if (result == null) {
			return false
		} else {
			result.password = "******"
			return result
		}
	})
};


// Set User as Admin 
module.exports.setAdmin = (reqBody) => { 
 
	let setAdmin = { 
		isAdmin: true 
	} 
 
	
	return User.findOne({email: reqBody.email}).then(result => { 
 
		if(result.isAdmin){ 
 
			return false
		} else { 
 
			return User.findByIdAndUpdate(result._id, setAdmin).then((setUserAdmin, error) => { 
 
				if(error){ 
 
					return false
				} else { 
 
					return ({success: true, message: "User successfully set as an Admin"})
				} 
			}) 
		} 
	}) 
}; 
 






