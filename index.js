const express = require ('express');
const mongoose = require ('mongoose');
const cors = require ('cors');
const userRoutes = require('./routes/userRoutes');
const roomRoutes = require('./routes/roomRoutes');
const transactionRoutes = require('./routes/transactionRoutes')


mongoose.set('strictQuery', true);


const app = express();
const port = process.env.PORT || 2000;


//MIDDLEWARE
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//URI
app.use('/users', userRoutes);
app.use('/rooms', roomRoutes);
app.use('/transactions',transactionRoutes);



//MONGOOSE CONECTION

mongoose.connect("mongodb+srv://napmarcos:admin123@marcos.hsczop7.mongodb.net/HOTEL_BOOKING?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", () => console.error('Conection Error'));
db.once("open", () => console.log('Connected to MongoDB'))









app.listen(port, () => {console.log(`API is now running at port ${port}`)})
